//! Provides a macro for accessing data defined in the `package.metadata` section of the
//! Cargo manifest.  Please refer to the documentation of `cargo_meta_proc`.

use std::env;

pub use cargo_meta_proc::metadata as package_metadata;

/// Re-run builds when the Cargo manifest is updated
///
/// # Example
/// Add `cargo_meta` to the `build-dependencies` section of `Cargo.toml`.
/// In your `build.rs`:
/// ```no_run
/// fn main() {
///     cargo_meta::rerun_on_manifest_change();
/// }
/// ```
pub fn rerun_on_manifest_change() {
    let manifest = format!("{}/Cargo.toml", env::var("CARGO_MANIFEST_DIR").unwrap());
    println!("cargo:rerun-if-changed={}", manifest);
}

/// Used internally for type assertions
pub const fn id<T>(x: T) -> T {
    x
}
